package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class D4_63263_RAKESHApplication {

	public static void main(String[] args) {
		SpringApplication.run(D4_63263_RAKESHApplication.class, args);
	}

}

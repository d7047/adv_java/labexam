package com.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity

@Getter
@Setter
@ToString

public class Product extends BaseEntity {
@Column(length = 30)
private String name;
@Column(length = 100)
private String desc;

private int Qnty;

private int price;
@ManyToOne
@JoinColumn(nullable = false)
private User user;

}

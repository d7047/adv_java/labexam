package com.app.service;

import java.util.List;

import com.app.entities.Product;

public interface IProductService {
 List<Product>getProductsByUserId(Long UserId);
Product  save(Product p);
List<Product> getAllProducts();
}

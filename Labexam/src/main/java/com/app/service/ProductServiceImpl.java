package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.IProductRepository;
import com.app.entities.Product;

@Service
@Transactional
public class ProductServiceImpl implements IProductService {
@Autowired
private  IProductRepository prodRepo;
	@Override
	public List<Product> getProductsByUserId(Long UserId) {
		// TODO Auto-generated method stub
		return prodRepo.findByUserId(UserId);
	}

	@Override
	public Product save(Product p) {
		
		return prodRepo.save(p);
	}

	@Override
	public List<Product> getAllProducts() {
		
		return prodRepo.findAll();
	}

}

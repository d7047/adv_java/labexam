package com.app.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.entities.User;
import com.app.service.IProductService;
import com.app.service.IUserService;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping("/user")
public class UserController {
@Autowired
private IProductService productService;
@Autowired
private IUserService userService;
@GetMapping("/home")
public String showProducts(HttpSession session,Model map)
{
	User user=(User)session.getAttribute("user");
	map.addAttribute("products", productService.getProductsByUserId(user.getId()));
	return "/user/home";
}

}
